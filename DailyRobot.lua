package.path = package.path .. ";" .. getScriptPath() .. "\\?.lua"
package.cpath = package.cpath .. ";" .. getWorkingFolder() .. "\\?51.dll"



require "QL"
require ".\\settings\\DailyRobot\\settings"


dofile(getScriptPath().."\\include\\tools.lua")



make_trans=true    --  true - ������� ����� , false  - �������� ����� ( �������� �� ������������ )

is_run = true
robot_is_run =false 
in_transaction=false

operation=nil
operation_01=nil
trans_id=nil
order_num=nil


curr_block=0
cbb_index=0
cbb_index_b1=0
cbb_index_b2=0

local tm_prev=os.clock()
local tm_curr=os.clock()
local wlog=nil


local   fcl_record=nil
local   fch_record=nil






logmaxlines=Settings.log_max_lines or 1000
shrincfunk=getShrinkFileFunc(log,oldlog,logmaxlines)


function  AddMess(msg)


--[[
    if wlog:IsClosed()
    then
        wlog:Show()
    end

    local row=wlog:AddLine()
    wlog:SetValue(row,"date",os.date())
    wlog:SetValue(row,"message",msg)

    wlog:Highlight(row,nil,RED,nil,500)
    ]]
    message(msg,1)
    shrincfunk()
    toLog(log,msg)

end



function main()
	if isConnected()
    then
	    InitEMA()
	    StopRobot()
	end

    while is_run do
	    tm_curr=os.clock()
	    step()
        sleep(50)
    end
end



function OnInit()

    message(" interval >>  "..Settings.interval,2)

--[[
	wlog = QTable.new()
 	wlog:AddColumn("date", QTABLE_STRING_TYPE, 20)
  	wlog:AddColumn("message", QTABLE_STRING_TYPE, 100)
	wlog:SetCaption("Log Window")
   	wlog:Show()


]]

end





function  myCreateDS(_interval,_cb)
local   _ds,_err
    _ds,_err=CreateDataSource(Settings.class_code,Settings.sec_code,  _interval )
    assert(_ds,_err)
--        AddMess("Settings > "..table2string(Settings))
--        AddMess(ds)
    local   res=_ds:SetUpdateCallback(_cb)
    return _ds
end






function InitEMA()


        tm_prev=os.clock()
        tm_curr=os.clock()

        ds,err=CreateDataSource(Settings.class_code,Settings.sec_code,  Settings.interval )
        assert(ds,err)
--        AddMess("Settings > "..table2string(Settings))
--        AddMess(ds)
        res=ds:SetUpdateCallback(cbb)




        myMACD=getMACD(Settings.block_1.alg_1.Nshort,Settings.block_1.alg_1.Nlong,Settings.value_type)
        mySTAH_K=get_STAH_K(Settings.block_1.alg_2.sth_n,Settings.block_1.alg_2.sth_m)

        myMACD_b2=getMACD(Settings.block_2.alg_1.Nshort,Settings.block_2.alg_1.Nlong,Settings.value_type)

        myEMA_b2=getEMA(Settings.block_2.alg_2.period,Settings.value_type)
        --    myEMA_b2=cached_EMA( Settings.block_2.alg_2.period,Settings.value_type,function(indx)  return dValue(indx,_type) end  )







        --[[

            ds_b1,err_b1=CreateDataSource(Settings.class_code,Settings.sec_code,  Settings.block_1.interval )
            assert(ds_b1,err_b1)
            res_b1=ds_b1:SetUpdateCallback(cbb_b1)

            ds_b2,err_b2=CreateDataSource(Settings.class_code,Settings.sec_code,  Settings.block_2.interval )
            assert(ds_b2,err_b2)
            res_b2=ds_b2:SetUpdateCallback(cbb_b2)

        ]]


        ds_b1= myCreateDS(Settings.block_1.interval,cbb_b1)
        ds_b2= myCreateDS(Settings.block_2.interval,cbb_b2)





        --    priceEMA_b1=getEMA(Settings.block_1.ema_period,Settings.block_1.value_type)
    --    priceEMA_b2=getEMA(Settings.block_2.ema_period,Settings.block_2.value_type)
    priceEMA_b1=getEMA_M(Settings.block_1.ema_period,Settings.block_1.value_type, ds_b1)
    priceEMA_b2=getEMA_M(Settings.block_2.ema_period,Settings.block_2.value_type, ds_b2)


end


function StopRobot()
	robot_is_run=false
	tm_prev=os.clock()
	tm_curr=os.clock()
    in_transaction=false
    trans_id=nil
end

function OnConnected()
    AddMess("Connect")
	InitEMA()
	StopRobot()
end

function OnDisconnected()
    AddMess("Disconnect")
	robot_is_run=false
    in_transaction=false
	tm_prev=os.clock()
	tm_curr=os.clock()
end


function step()

    if not is_run  then    return end

    if not isConnected()
    then
        return
    end

  	if robot_is_run
    then
        if (tm_curr-tm_prev)<Settings.timeout_trans
        then
            return
        end
    else
        if (tm_curr-tm_prev)<Settings.delay_start_robot
        then
            return
        end
        robot_is_run=true
        AddMess("Robot started")
	end


    tm_prev=tm_curr
    reget_futures_holding()
   reget_futures_limits()
--    if not is_run  then    return end
    maketrans()

end



function OnStop()
--    wlog:delete()
    is_run = false
end



function  OnTransReply(tbl)
    AddMess("OnTransReply ??> "..table2string(tbl))
    if not in_transaction        then    return  end
    if tbl.trans_id==trans_id and tbl.status==3 then
        order_num=tbl.order_num
        AddMess("OnTransReply >>> "..table2string(tbl))
    end
end


function  OnOrder(tbl)
    AddMess("OnOrder >> "..table2string(tbl))
end



function  OnTrade(tbl)
    AddMess("OnTrade >> "..table2string(tbl))
    onTrade_01(tbl)
end

function    onTrade_01(tbl)

    if not make_trans           then    return end

    if curr_block~=1            then    return  end
    if not in_transaction       then    return  end
    if trans_id==nil            then    return  end
    if order_num==nil           then    return  end
    if order_num~=tbl.order_num then    return  end



    local    prm_operation="B"
    if operation_01=="B"    then    prm_operation="S" end


    proskal_tp=Settings.proskal_tp
    proskal_sl=Settings.proskal_sl

    offset=Settings.offset
    spread=Settings.spread




    --       ema_curr    -- ??????????    onTrade
    stopprice=tbl.price-proskal_tp
    stopprice2=tbl.price+proskal_sl
    price=stopprice2+Settings.otstup

    if prm_operation=="S"
    then
        stopprice=tbl.price+proskal_tp
        stopprice2=tbl.price-proskal_sl
        price=stopprice2-Settings.otstup
    end

    local    units="PERCENTS"
    if  Settings.units==0
    then
        units="PRICE_UNITS"
    end

    _,rslt = sendTakeProfitAndStopLimit(
        Settings.class_code ,       --  class
        Settings.sec_code ,         --  security
        prm_operation ,             --  direction
        price ,                         --  price
        stopprice ,                         --  stopprice
        stopprice2 ,                       --  stopprice2

--        Settings.quantity ,         --  volume
        tbl.qty,                    --  �� ��������� ������

        offset ,                         --  offset
        units ,                --  offsetunits
        spread ,                         --  deffspread
        units ,                --  deffspreadunits
        Settings.account ,          --  account
        "TODAY" ,                   --  exp_date
        nil ,                       --  client_code
        nil                         --  comment
    )

    AddMess(">>>>>     stopprice="..toPrice(Settings.sec_code,stopprice,Settings.class_code).."     stopprice2="..toPrice(Settings.sec_code,stopprice2,Settings.class_code))

    AddMess( rslt )

    trans_id=nil
    order_num=nil
    in_transaction=false

    restart_cycle()
end



--[[

function  OnFuturesLimitChange(tbl)
    if  in_transaction  then    return end
    AddMess("OnFuturesLimitChange >> "..table2string(tbl))
    local evntmsg="OnFuturesLimitChange trdaccid(account)="..tbl.trdaccid.."  limit_type="..
        tbl.limit_type.."     varmargin="..tbl.varmargin.."  accruedint=".. tbl.accruedint..
        "  cbplused="..tbl.cbplused
    AddMess(evntmsg)
    restart_cycle()
end

function  OnFuturesClientHolding(tbl)
    if  in_transaction  then    return end

    AddMess("OnFuturesClientHolding >> "..table2string(tbl))
    local evntmsg="OnFuturesClientHolding trdaccid(account)="..tbl.trdaccid.."  type="..
        tbl.type.."     totalnet="..tbl.totalnet
    AddMess(evntmsg)
    restart_cycle()
end


]]


function    search_futures_limits(t)
    return  t.trdaccid==Settings.account and t.limit_type==0
end

function    search_futures_holding(t)
    return  t.trdaccid==Settings.account   and t.type==0
end

function    reget_futures_holding()
    AddMess("reget_futures_holding  !!!  ")
    fch_record=nil
    nrecs=getNumberOf("futures_client_holding")
    if nrecs==0 then
        AddMess(" futures_client_holding  no records " )
        return
    end

    recs=SearchItems("futures_client_holding",0, nrecs-1,search_futures_holding)
    if recs==nil
    then
        AddMess(" futures_client_holding by filter no records ")
        return
    end

    fch_record=getItem("futures_client_holding",recs[1])
    if fch_record==nil
    then
        AddMess(" futures_client_holding  fch_record=nil ")
        return
    end

    local msg0="fch_record  totalnet="..fch_record.totalnet
    AddMess(msg0)
    AddMess("futures_client_holding fch_record="..table2string(fcl_record) )
end

function    reget_futures_limits()
    fcl_record=nil

    nrecs=getNumberOf("futures_client_limits")
    if nrecs==0 then
        AddMess(" futures_client_limits  no records " )
        return
    end

    recs=SearchItems("futures_client_limits",0, nrecs-1,search_futures_limits)
    if recs==nil
    then
        AddMess(" futures_client_limits by filter no records ")
        return
    end

    fcl_record=getItem("futures_client_limits",recs[1])
    if fcl_record==nil
    then
        AddMess(" futures_client_limits  fcl_record=nil ")
        return
    end

    local msg0="fcl_record  cbplused="..fcl_record.cbplused.."   varmargin="..fcl_record.varmargin.."     accruedint="..fcl_record.accruedint
    AddMess(msg0)
    AddMess("futures_client_limits fcl_record="..table2string(fcl_record) )


    local  sm=fcl_record.varmargin + fcl_record.accruedint

    if sm < Settings.sum_min
    then
        is_run=false
        AddMess("Control block "..fcl_record.varmargin.." + "..fcl_record.accruedint.." < "..Settings.sum_min)
        return
    end

    if sm > Settings.sum_max
    then
        is_run=false
        message("Control block "..fcl_record.varmargin.." + "..fcl_record.accruedint.." > "..Settings.sum_max)
        return
    end
end




function    restart_cycle()
    AddMess(" restart cycle")
    tm_prev=tm_curr-Settings.timeout_trans+5
end



function	maketrans()

--    if  in_transaction and trans_id~=nil and order_num==nil
    if  in_transaction
    then
        _,lrez=killAllOrders({ACCOUNT=Settings.account})
        AddMess("Kill order !!!!")
        AddMess(lrez)
        in_transaction=false
        trans_id=nil
    end


    if (in_transaction)
    then
        AddMess("maketrans  return because in transaction")
        return
    end


    curr_block=1
    if  (fch_record~=nil) and  (fch_record.totalnet~=0)
    then
        curr_block=2
    end

--    curr_block=2

    DoBlock_1()
    DoBlock_2()


end

function    DoBlock_1()
    if (curr_block~=1)  then    return       end
    local   rez=DoBlock_1_alg_1()
--    AddMess("DoBlock_1_alg_1()=>>"..rez.."<<")
    if (not rez)
    then
        rez=DoBlock_1_alg_2()
--        AddMess("DoBlock_1_alg_2()="..rez)
    end
end

function    DoBlock_1_alg_1()
    local   index=cbb_index
    local   spream="Block_1 alg_1  >> "
    local   alg_pref=Settings.block_1.alg_pref
--    AddMess(spream.."  01")
    if  (alg_pref==2)   then    return  false end

--    AddMess(spream.."  02")
    local   stngs=Settings.block_1.alg_1
    local   distance=stngs.distance
    local   DeltaMACD=stngs.DeltaMACD

    local   macd_curr  = myMACD(index)
    local   macd_prev  = myMACD(index)


--    AddMess(spream.."  03")
    if index>distance
    then
        macd_prev = myMACD(index-distance)
    end

    if  macd_curr==nil or macd_prev==nil  then    return  false  end

    local   delta=round(macd_curr-macd_prev,4)

--    AddMess(spream.."  04")

    local ap_delta=math.abs(delta)
    local s_delta=math.abs(DeltaMACD)

    if  ap_delta<s_delta
    then
        AddMess(spream.."ap_delta="..ap_delta.."      s_delta="..s_delta)
        return  false
    end


    local msg=spream.." macd_prev="..macd_prev.." macd_curr="..macd_curr
    AddMess(msg)

    local   operation="S"
    if (delta>0)
    then
        operation="B"
    end

    local   rez=PutOrder_Block_1(operation,spream)
    return  rez==""
end

function    DoBlock_1_alg_2()
    local   index=cbb_index
    local   spream="Block_1 alg_2  >> "
    local   alg_pref=Settings.block_1.alg_pref
    if      (alg_pref==1)   then    return  false end

    local   stngs=Settings.block_1.alg_2

    local   sth_k_curr=mySTAH_K(index)

    local   high=stngs.high
    local   low=stngs.low

    if ( (sth_k_curr<low) and (sth_k_curr>high))
    then
        return  false
    end


    local msg="  sth_k_curr"..sth_k_curr.."  high="..high.."  low="..low
    AddMess(spream..msg)

    local   operation="S"
    if (sth_k_curr<=high)
    then
        operation="B"
    end

    local   rez=PutOrder_Block_1(operation,spream)
    return  rez==""


end

function    DoBlock_2()
    if (curr_block~=2)  then    return       end
    local   rez=DoBlock_2_alg_1()
    if (not rez)
    then
        rez=DoBlock_2_alg_2()
    end
end

function    DoBlock_2_alg_1()
    local   index=cbb_index
    local   spream="Block_2 alg_1  >> "
    local   alg_pref=Settings.block_2.alg_pref
    if  (alg_pref==2)   then    return  false end

    local   totalnet=fch_record.totalnet
    local   stngs=Settings.block_2.alg_1
    local   distance=stngs.distance
    local   RazvorotIL=stngs.RazvorotIL


    AddMess("DoBlock_2_alg_1   index="..index)


    local   macd_curr  = myMACD_b2(index)
    local   macd_prev  = macd_curr


    if index>distance
    then
        macd_prev = myMACD_b2(index-distance)
    end

    if  macd_curr==nil or macd_prev==nil  then    return  false  end

    local   delta=round(macd_curr-macd_prev,4)


    local   ap_delta=math.abs(delta)
    local   s_delta=math.abs(RazvorotIL)
    local   ind_1=(delta<0 and totalnet<0) or (delta>0 and totalnet>0)
    local   ind_2=(ap_delta<s_delta)
    local   ind_all=ind_1 and ind_2

    if  (not ind_all)
    then
        AddMess(spream.."ap_delta="..ap_delta.."      s_delta="..s_delta.."  totalnet="..totalnet)
        return  false
    end


    local msg=spream.." macd_prev="..macd_prev.." macd_curr="..macd_curr
    AddMess(msg)

    local   operation="B"
    if (delta>0)
    then
        operation="S"
    end

    local   rez=PutOrder_Block_2(operation,spream)
    return  rez==""

end

function    DoBlock_2_alg_2()

    local   index=cbb_index
    local   spream="Block_2 alg_2  >> "
    local   alg_pref=Settings.block_2.alg_pref
    if  (alg_pref==1)   then    return  false end

    local   totalnet=fch_record.totalnet
    local   stngs=Settings.block_2.alg_2
    local   delta=stngs.delta

    AddMess("DoBlock_2_alg_2  1")
    AddMess("DoBlock_2_alg_2   index="..index.." cbb_index"..cbb_index)
    local   p_avg  = myEMA_b2(index)
    local   p_curr=dValue(index,Settings.value_type)
--    local   p_avg  = p_curr  --  ????

    AddMess("DoBlock_2_alg_2  2")


    --    if  macd_curr==nil or macd_prev==nil  then    return  false  end

    local   s_delta=round(p_curr-p_avg,4)


    local   ind_1=(s_delta<0 and totalnet<0) or (s_delta>0 and totalnet>0)
    local   ind_2=(math.abs(delta)<math.abs(s_delta))
    local   ind_all=ind_1 and ind_2

    if  (not ind_all)
    then
        AddMess(spream.."delta="..delta.."      s_delta="..s_delta.."  totalnet="..totalnet)
        return  false
    end


    local msg=spream.." p_avg="..p_avg.." p_curr="..p_curr
    AddMess(msg)

    local   operation="B"
    if (s_delta>0)
    then
        operation="S"
    end

    local   rez=PutOrder_Block_2(operation,spream)
    return  rez==""

end

function    PutOrder_Block_1(operation,spream)
    local   index=cbb_index
    local   index_b1=cbb_index_b1

    local   price_m0=dValue(index,Settings.value_type)
    local   price_m=priceEMA_b1(index_b1)

    local   proskal=Settings.block_1.proskal
    local   price=price_m

    operation_01=operation

    AddMess("PutOrder_Block_1 index_b1="..index_b1.."  price_m(EMA)="..price_m.."  price_m(dValue)="..price_m0)

    local msg=""

    if (operation=="S")
    then
        msg=" Sale "..msg
        price=price-proskal
    end


    if (operation=="B")
    then
        msg=" Buy "..msg
        price=price+proskal
    end

    AddMess("PutOrder_Block_1 index="..index.."  price="..price)

    local   prms={}
    prms["price"]=price
    prms["operation"]=operation
    prms["quantity"]=Settings.quantity
    prms["spream"]=spream
    prms["EXECUTION_CONDITION"]="KILL_BALANCE"

    local   lkod=PutOrder(prms)


    if trim(lkod)==trim("")
    then
        in_transaction=true
        order_num=nil
    else
        restart_cycle()
    end


    return  lkod

end

function    PutOrder_Block_2(operation,spream)
    local   index=cbb_index
    local   index_b2=cbb_index_b2
    local   totalnet=fch_record.totalnet

    local   price_m0=dValue(index,Settings.value_type)
    local   price_m=priceEMA_b2(index_b2)

    local   proskal=Settings.block_2.proskal
    local   price=price_m


    _,lrez=killAllStopOrders({ACCOUNT=Settings.account})
    AddMess(spream.."Kill all stop orders !!!!  lrez="..lrez)


    AddMess("PutOrder_Block_2 index_b2="..index_b2.."  price_m(EMA)="..price_m.."  price_m(dValue)="..price_m0)

    local msg=""
    if (operation=="S") then
        msg=" Sale "..msg
        price=price-proskal
    end
    if (operation=="B") then
        msg=" Buy "..msg
        price=price+proskal
    end

    AddMess("PutOrder_Block_2 index="..index.."  price="..price)

    local   prms={}
    prms["price"]=price
    prms["operation"]=operation
    prms["quantity"]=math.abs(totalnet)
    prms["spream"]=spream
    prms["EXECUTION_CONDITION"]="FILL_OR_KILL"

    AddMess("PutOrder_Block_2 step_1 price="..price )
    local   lkod=PutOrder(prms)

    if lkod~=""
    then
        if (operation=="S") then
            msg=" Sale "..msg
            price=price-proskal
        end
        if (operation=="B") then
            msg=" Buy "..msg
            price=price+proskal
        end
        prms["price"]=price
        AddMess("PutOrder_Block_2 step_2 price="..price )
        local   lkod=PutOrder(prms)
    end


    return  lkod

end

function    PutOrder( prms )
    local   spream=prms.spream
    trans_id=random_max()
    local   trans={}
    trans["ACCOUNT"]     = Settings.account
    trans["CLIENT_CODE"] = Settings.account
    trans["TRANS_ID"]    = tostring(trans_id)
    trans["CLASSCODE"]   = Settings.class_code
    trans["SECCODE"]     = Settings.sec_code
    trans["ACTION"]      = "NEW_ORDER"
    trans["OPERATION"]   = prms.operation
    trans["QUANTITY"]    = tostring(prms["quantity"])
    trans["TYPE"]    = "M"
    trans["PRICE"]   = toPrice(Settings.sec_code,prms.price,Settings.class_code)
    if prms["EXECUTION_CONDITION"]~=nil
    then
        trans["EXECUTION_CONDITION"] = prms["EXECUTION_CONDITION"]
    end



    local lkod="????"

    if make_trans
    then
        lkod=sendTransaction(trans)
    end



    local mess=spream.."\n"..
            "ACCOUNT="..trans["ACCOUNT"].."\n"..
            "CLIENT_CODE="..trans["CLIENT_CODE"].."\n"..
            "TRANS_ID="..trans["TRANS_ID"].."\n"..
            "CLASSCODE="..trans["CLASSCODE"].."\n"..
            "SECCODE="..trans["SECCODE"].."\n"..
            "ACTION="..trans["ACCOUNT"].."\n"..
            "OPERATION="..trans["OPERATION"].."\n"..
            "QUANTITY="..trans["QUANTITY"].."\n"..
            "TYPE="..trans["TYPE"].."\n"..
            "PRICE="..trans["PRICE"].."\n".."\n"..
            "seconds="..(os.clock()-tm_prev).."\n".."\n"

    if trans["EXECUTION_CONDITION"]~=nil
    then
        mess=mess.."EXECUTION_CONDITION="..trans["EXECUTION_CONDITION"].."\n".."\n"
    end

    mess=mess.."Result="..lkod

    AddMess(mess)
    if lkod==nil  then  lkod="???"   end


    return  lkod



end







function  cbb(index)

    cbb_index=index

    --    AddMess("cbb cbb_index="..cbb_index)

    local   xtmp=0
    xtmp  = myMACD(index)
    xtmp  = mySTAH_K(index)
    xtmp  = myMACD_b2(index)
    xtmp  = myEMA_b2(index)

    --    xtmp  = priceEMA_b1(index)
    --    xtmp  = priceEMA_b2(index)
end




function  cbb_b1(index)
    cbb_index_b1=index
    local   xtmp=0
    xtmp  = priceEMA_b1(index)
end


function  cbb_b2(index)
    cbb_index_b2=index
    local   xtmp=0
    xtmp  = priceEMA_b2(index)
    AddMess("cbb_b2  index="..index)
end


