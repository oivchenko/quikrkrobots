log=getScriptPath().."\\DailyRobLog.log"
oldlog=getScriptPath().."\\prevlogs\\DailyRobLog.log"



Settings=
{

    class_code="SPBFUT",
    sec_code="SiU4",
    account="SPBFUT00BT4",     -- Account


    quantity=2,        -- ����������
    value_type = "C",  -- EMA  type


    delay_start_robot=20,   --  �������� ������� ������
    timeout_trans=15,       --  ������� ���������� ����������


    interval=INTERVAL_M1,

    log_max_lines=2000,     --  ���������������-�� ����� � ��� ���� ��������� ��� ���������� � prevlogs � ��������� ������
                            --  ��� ���������� ���������  ���� ���-�� = 1000


--[[
    ������
    ��������������
    �����������
     ]]


    proskal_sl=12,
    proskal_tp=8,
    otstup=4,           --  ���� ��������� ����-����� +-  ������

    offset=2,   --  ������ ��  max
    spread=2,   --  �������� �����

    units=0,    --       0 - "PRICE_UNITS"  ,   1 - "PERCENTS"

    sum_min=-300,
    sum_max=2000,



    block_1=
    {
        proskal=20,
        alg_pref=0,  -- 0 - ����������� ���������� ����� ���������� , 1 - ������ ������ , 2 - ������ ������

        ema_period=12,
        interval=INTERVAL_M1,
        value_type = "C",

        alg_1=
        {
            --   MACD  settings
            Nlong=26,
            Nshort=12,
            distance = 6,
            DeltaMACD=0.003    -- DeltaMACD
        },
        alg_2=
        {
            --  stahostic  settings
            sth_m=9,   --  �����������
            sth_n=9,   --  ����� �������� (������ �� 5 �� 21)
            high=30,
            low =75
        }
    },



    block_2=
    {
	    proskal=20,
        alg_pref=0,  -- 0 - ����������� ���������� ����� ���������� , 1 - ������ ������ , 2 - ������ ������

        ema_period=12,
        interval=INTERVAL_M1,
        value_type = "C",

        alg_1=
        {
            Nlong=9,
            Nshort=2,
            distance = 6,
            RazvorotIL=0.003
        },
        alg_2=
        {
            period=12,
            delta=1
        }
    }

}
