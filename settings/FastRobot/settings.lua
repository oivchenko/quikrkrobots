log=getScriptPath().."\\FastRobLog.log"
oldlog=getScriptPath().."\\prevlogs\\FastRobLog.log"

--   ��������� EMA ��� ���� � ����� ����� ����� ( ���� EMA_price )

Settings=
{

    class_code="SPBFUT",
    sec_code="SiU4",
    account="SPBFUT00BT4",     -- Account


    quantity=1,             -- ����������
    value_type = "C",       -- EMA  type

    period = 12,
    distance = 6, 

    delay_start_robot=20,   --  �������� ������� ������
    timeout_trans=30,       --  ������� ���������� ����������


    interval=INTERVAL_M1,

    log_max_lines=2000,     --  ���������������-�� ����� � ��� ���� ��������� ��� ���������� � prevlogs � ��������� ������
                            --  ��� ���������� ���������  ���� ���-�� = 1000

    s_delta=0,          -- Delta
    proskal=20,        	-- ���������������

    proskal_sl=12,
    proskal_tp=8,
    otstup=4,           --  ���� ��������� ����-����� +-  ������

    offset=2,   --  ������ ��  max
    spread=2,   --  �������� �����

    units=0,    --       0 - "PRICE_UNITS"  ,   1 - "PERCENTS"

    sum_min=-300,
    sum_max=2000,



    EMA_price=
    {
        value_type = "C",       -- EMA  type
        period = 12,
        interval=INTERVAL_M1
    }


}
